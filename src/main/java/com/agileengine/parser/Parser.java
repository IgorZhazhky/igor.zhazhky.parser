package com.agileengine.parser;

import org.jsoup.nodes.Attribute;

import java.util.List;

/**
 * Object that search
 */
public interface Parser {

    /**
     * {@inheritDoc}
     */
    List<Attribute> getAttributesFromPage(String path, String targetElementId);
}
