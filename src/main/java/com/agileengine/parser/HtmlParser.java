package com.agileengine.parser;

import com.agileengine.utils.Constants;
import com.agileengine.utils.jsoup.JsoupFindByIdSnippet;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of Parser interface for html pages
 */
public class HtmlParser implements Parser {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Attribute> getAttributesFromPage(String path, String targetElementId) {
        Optional<Element> elementOpt = Optional.ofNullable(
                JsoupFindByIdSnippet.findElementById(new File(path), targetElementId))
                .orElseThrow(() -> new IllegalArgumentException(Constants.CANT_GET_ELEMENT_BY_ID));

        return getAttributesFromElement(elementOpt.get());
    }

    private List<Attribute> getAttributesFromElement(Element element) {
        return element.attributes().asList();
    }


}
