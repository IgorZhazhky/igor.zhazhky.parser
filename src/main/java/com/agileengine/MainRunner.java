package com.agileengine;

import com.agileengine.finder.Finder;
import com.agileengine.finder.HtmlFinder;
import com.agileengine.parser.HtmlParser;
import com.agileengine.parser.Parser;
import org.jsoup.nodes.Attribute;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Starts the html parser program
 */
public class MainRunner {

    public static void main(String[] args) {
        Parser parser = new HtmlParser();
        Finder finder = new HtmlFinder();
        List<Attribute> elementAttributes = parser.getAttributesFromPage(args[0], args[1]);

        List<File> targetPages =
                Arrays.stream(Arrays.copyOfRange(args, 2, args.length)).map(File::new).collect(Collectors.toList());
        finder.findAppropriateElementByAttributes(elementAttributes, targetPages);

    }
}
