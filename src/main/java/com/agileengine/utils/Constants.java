package com.agileengine.utils;

public final class Constants {
    private Constants() {
        throw new IllegalStateException("Can't be initialized");
    }

    public static final String CHARSET_NAME = "utf8";

    public static final String CANT_GET_ELEMENT_BY_ID = "Can't get element from html by id";
}
