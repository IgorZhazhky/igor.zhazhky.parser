package com.agileengine.finder;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;

import java.io.File;
import java.util.List;

/**
 * Searches for the most similar element in all target html pages by attributes of given one
 */
public interface Finder {

    /**
     * Returns the most similar element found from all html files
     *
     * @param attributes list of element attributes for check
     * @param targetPages all html pages the most similar element will be searched from
     * @return the most similar element found from all html files
     */
    Element findAppropriateElementByAttributes(List<Attribute> attributes, List<File> targetPages);
}
