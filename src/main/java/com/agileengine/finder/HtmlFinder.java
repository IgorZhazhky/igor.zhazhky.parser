package com.agileengine.finder;

import info.debatty.java.stringsimilarity.JaroWinkler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.agileengine.utils.Constants.CHARSET_NAME;

/**
 * Implementation of Finder interface
 */
public class HtmlFinder implements Finder {

    private static final Double DEFAULT_NO_SIMILARITY_VALUE = Double.MAX_VALUE;
    private static final Logger LOGGER = Logger.getLogger(HtmlFinder.class.getName());
    private JaroWinkler algorithm;

    public HtmlFinder() {
        algorithm = new JaroWinkler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Element findAppropriateElementByAttributes(List<Attribute> attributes, List<File> targetPages) {

        List<Elements> targetElements = targetPages.stream()
                .map(file -> {
                    try {
                        return Jsoup.parse(file, CHARSET_NAME, file.getAbsolutePath()).getAllElements();
                    } catch (IOException e) {
                        LOGGER.warning(e.getMessage());
                    }
                    return null;
                }).collect(Collectors.toList());

        Map<Element, Double> similarityMap = new HashMap<>();
        targetElements.forEach(elements -> similarityMap.putAll(
                retrieveSimilarityMapFromPagesByAttribute(attributes, elements)));

        return similarityMap.entrySet().stream().min(Comparator.comparingDouble(Map.Entry::getValue)).get().getKey();
    }

    /**
     * Returns the map of elements and their similarity indexes
     * that are calculated by all given attributes from entry list
     *
     * @param attributes      attributes list of main page element
     * @param elementsFromDoc list of element that need to be checked on similarity
     * @return map of elements and their similarity indexes
     */
    private Map<Element, Double> retrieveSimilarityMapFromPagesByAttribute(List<Attribute> attributes,
                                                                           List<Element> elementsFromDoc) {
        Map<Element, Double> similarityMap = new HashMap<>();
        for (Attribute attribute : attributes) {
            elementsFromDoc.forEach(element -> similarityMap.put(element, calculateSimilarity(attribute, element)));
        }
        return similarityMap;
    }

    /**
     * Uses Jaro Winkler algorithm to calculate difference of strings in attributes
     *
     * @param attribute string attribute to be checked
     * @param targetElement element for check
     * @return double difference between 2 strings (more symbols in strings deffer - higher return value)
     * or returns DEFAULT VALUE if elements  doesn't contain such attribute
     */
    private double calculateSimilarity(Attribute attribute, Element targetElement) {
        if (targetElement.hasAttr(attribute.getKey())) {
            return algorithm.similarity(
                    targetElement.attr(attribute.getKey()), attribute.getValue());
        }
        return DEFAULT_NO_SIMILARITY_VALUE;
    }
}
