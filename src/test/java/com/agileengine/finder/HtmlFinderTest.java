package com.agileengine.finder;

import com.agileengine.parser.HtmlParser;
import com.agileengine.parser.Parser;
import org.jsoup.nodes.Attribute;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(Parameterized.class)
public class HtmlFinderTest {

    private Finder finder;
    private Parser parser;

    @Parameterized.Parameter
    public String originPath;
    @Parameterized.Parameter(1)
    public String elementId;
    @Parameterized.Parameter(2)
    public String result;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new String[][]{
                {"./pages/origin.html",
                        "make-everything-ok-button",
                        "Make everything OK"}
        });
    }

    @Before
    public void setUp() {
        parser = new HtmlParser();
        finder = new HtmlFinder();
    }

    @Test
    public void calculateTest() {
        List<Attribute> attributesFromPage = parser.getAttributesFromPage(originPath, elementId);
        String[] files = new String[] {"./pages/sample1.html", "./pages/sample2.html", "./pages/sample3.html", "./pages/sample4.html"};
        List<File> targetPages =
                Arrays.stream(files).map(File::new).collect(Collectors.toList());
        Assert.assertEquals(result, finder.findAppropriateElementByAttributes(attributesFromPage, targetPages).text());

    }
}