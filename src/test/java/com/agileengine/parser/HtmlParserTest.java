package com.agileengine.parser;

import org.jsoup.nodes.Attribute;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class HtmlParserTest {

    private Parser parser;

    @Parameterized.Parameter
    public String originPath;
    @Parameterized.Parameter(1)
    public String elementId;
    @Parameterized.Parameter(2)
    public String result;


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new String[][]{
                {"./pages/origin.html",
                        "make-everything-ok-button",
                        "btn btn-success"}
        });
    }

    @Before
    public void setUp() {
        parser = new HtmlParser();
    }

    @Test
    public void calculateTest() {
        List<Attribute> attributesFromPage = parser.getAttributesFromPage(originPath, elementId);
        Assert.assertEquals(result, attributesFromPage.get(1).getValue());
    }
}